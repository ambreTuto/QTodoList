#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QtGlobal>

#if QT_VERSION >= 0x050000
#include <QtWidgets/QMainWindow>
#else
#include <QtGui/QMainWindow>
#endif

#include <QMap>
#include <memory>

class CTodoListManager;
class CTaskBo;
class FormTask;
class TaskListModel;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

public Q_SLOTS:
    void addTask();
    void displayTask(const CTaskBo &task);
    void askToRemoveATask(const qint64 taskId);
    void askToEditTask(const CTaskBo &task);
    void removeTaskFromBoard(const qint64 taskId);
    void updateTaskInBoard(const CTaskBo& task);
    void changeTaskState(const qint64 taskId);
    void displayError(const QString& reason);

protected:
    void changeEvent(QEvent *e);

private:
    FormTask* getFormTaskFromId(const qint64 taskId) const;

    Ui::MainWindow *ui;
    CTodoListManager *m_pTodoListManager;
    QMap<qint64, FormTask*> m_MapFormTask;
    std::shared_ptr<TaskListModel> modelNewTasks;
    std::shared_ptr<TaskListModel> modelOnGoingTasks;
    std::shared_ptr<TaskListModel> modelDoneTasks;
};

#endif // MAINWINDOW_H
