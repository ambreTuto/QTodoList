#ifndef DIALOGEDITTASK_H
#define DIALOGEDITTASK_H

#include <QtGlobal>

#if QT_VERSION >= 0x050000
#include <QtWidgets/QDialog>
#else
#include <QtGui/QDialog>
#endif

namespace Ui {
class DialogEditTask;
}

class DialogEditTask : public QDialog
{
    Q_OBJECT

public:
    explicit DialogEditTask(QWidget *parent = 0);
    ~DialogEditTask();
    void setName(const QString &name);
    QString getName() const;

protected:
    void changeEvent(QEvent *e);

private:
    Ui::DialogEditTask *ui;
};

#endif // DIALOGEDITTASK_H
