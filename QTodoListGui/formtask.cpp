#include "formtask.h"
#include "ui_formtask.h"
#include "dialogedittask.h"

#include <ctaskbo.h>

FormTask::FormTask(const CTaskBo& task, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::FormTask),
    m_pTask(new CTaskBo(task))
{
    ui->setupUi(this);
    ui->labelTaskName->setText(m_pTask->getName());
}

FormTask::~FormTask()
{
    delete ui;
    delete m_pTask;
}

qint64 FormTask::getId() const
{
    return m_pTask->getId();
}

void FormTask::updateTask(const CTaskBo &task)
{
    *m_pTask = task;
    ui->labelTaskName->setText(m_pTask->getName());
}

void FormTask::remove()
{
    emit removeTaskAsked(m_pTask->getId());
}

void FormTask::edit()
{
    DialogEditTask dialogEditBox(this);
    dialogEditBox.setName(m_pTask->getName());
    if(QDialog::Accepted == dialogEditBox.exec())
    {
        CTaskBo taskEdited(*m_pTask);
        taskEdited.setName(dialogEditBox.getName());
        emit editTaskAsked(taskEdited);
    }
}

void FormTask::changeEvent(QEvent *e)
{
    QWidget::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}
