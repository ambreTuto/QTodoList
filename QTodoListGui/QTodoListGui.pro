#-------------------------------------------------
#
# Project created by QtCreator 2017-05-07T21:28:35
#
#-------------------------------------------------


include ( ../QTodoListBin.pri )

QT       += core gui

CONFIG += c++14

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = QTodoListGui
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += main.cpp\
        mainwindow.cpp \
    dialogaddtask.cpp \
    formtask.cpp \
    dialogedittask.cpp \
    guiexception.cpp \
    tasklistmodel.cpp

HEADERS  += mainwindow.h \
    dialogaddtask.h \
    formtask.h \
    dialogedittask.h \
    helper/range.h \
    guiexception.h \
    tasklistmodel.h

FORMS    += mainwindow.ui \
    dialogaddtask.ui \
    formtask.ui \
    dialogedittask.ui

RESOURCES += \
    qtodolistgui.qrc

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../QTodoListBLL/release/ -lQTodoListBLL
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../QTodoListBLL/debug/ -lQTodoListBLL
else:unix: LIBS += -L$$OUT_PWD/../QTodoListBLL/ -lQTodoListBLL

INCLUDEPATH += $$PWD/../QTodoListBLL
DEPENDPATH += $$PWD/../QTodoListBLL

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../QTodoListBo/release/ -lQTodoListBo
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../QTodoListBo/debug/ -lQTodoListBo
else:unix: LIBS += -L$$OUT_PWD/../QTodoListBo/ -lQTodoListBo

INCLUDEPATH += $$PWD/../QTodoListBo
DEPENDPATH += $$PWD/../QTodoListBo
