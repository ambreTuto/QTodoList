#include "guiexception.h"

GuiException::GuiException(const QString &description)
    : m_description(description)
{

}

const char *GuiException::what() const noexcept
{
    return m_description.toStdString().c_str();
}
