#include "dialogaddtask.h"
#include "ui_dialogaddtask.h"

DialogAddTask::DialogAddTask(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DialogAddTask)
{
    ui->setupUi(this);
}

DialogAddTask::~DialogAddTask()
{
    delete ui;
}

QString DialogAddTask::getTaskName() const
{
    return ui->lineEditTaskName->text();
}

void DialogAddTask::changeEvent(QEvent *e)
{
    QDialog::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}
