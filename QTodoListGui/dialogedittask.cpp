#include "dialogedittask.h"
#include "ui_dialogedittask.h"

DialogEditTask::DialogEditTask(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DialogEditTask)
{
    ui->setupUi(this);
}

DialogEditTask::~DialogEditTask()
{
    delete ui;
}

void DialogEditTask::setName(const QString& name)
{
    ui->lineEditTaskName->setText(name);
}

QString DialogEditTask::getName() const
{
    return ui->lineEditTaskName->text();
}

void DialogEditTask::changeEvent(QEvent *e)
{
    QDialog::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}
