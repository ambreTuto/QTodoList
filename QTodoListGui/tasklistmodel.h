 #ifndef TASKLISTMODEL_H
#define TASKLISTMODEL_H

#include <QtCore/qglobal.h>
#include <QAbstractListModel>

typedef QPair<qint64, QString> TaskIdAndTitle;

class TaskListModel : public QAbstractListModel
{
    Q_OBJECT

public:
    TaskListModel();

    int rowCount(const QModelIndex &) const override;
    QVariant data(const QModelIndex &index, int role) const override;
    bool insertRows(int position, int rows, const QModelIndex &) override;
    bool setData(const QModelIndex &index, const QVariant &value, int role) override;
    Qt::DropActions supportedDropActions() const override;
    Qt::ItemFlags flags(const QModelIndex &index) const override;
    bool removeRows(int row, int count, const QModelIndex &parent = QModelIndex()) override;
    QStringList mimeTypes() const override;
    QMimeData *mimeData(const QModelIndexList &indexes) const override;
    bool canDropMimeData(const QMimeData *data, Qt::DropAction action, int row, int column, const QModelIndex &parent) const override;
    bool dropMimeData(const QMimeData *data, Qt::DropAction action, int row, int column, const QModelIndex &parent) override;

Q_SIGNALS:
    void taskStateChanged(const int id);

private:
    QList<TaskIdAndTitle> m_TaskList;
};

#endif // TASKLISTMODEL_H
