#ifndef FORMTASK_H
#define FORMTASK_H

#include <QtGlobal>

#if QT_VERSION >= 0x050000
#include <QtWidgets/QWidget>
#else
#include <QtGui/QWidget>
#endif

namespace Ui {
class FormTask;
}

class CTaskBo;

class FormTask : public QWidget
{
    Q_OBJECT

public:
    explicit FormTask(const CTaskBo &task, QWidget *parent = 0);
    ~FormTask();
    qint64 getId() const;
    void updateTask(const CTaskBo &task);

public Q_SLOTS:
    void remove();
    void edit();

protected:
    void changeEvent(QEvent *e);

private:
    Ui::FormTask *ui;
    CTaskBo* m_pTask;

Q_SIGNALS:
    void removeTaskAsked(const qint64 id);
    void editTaskAsked(const CTaskBo &task);
};

#endif // FORMTASK_H
