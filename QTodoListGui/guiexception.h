#ifndef GUIEXCEPTION_H
#define GUIEXCEPTION_H

#include <QException>

class GuiException : public QException
{
public:
    GuiException(const QString& description);
    virtual const char* what() const noexcept override;

protected:

private:
    const QString m_description;
};

#endif // GUIEXCEPTION_H
