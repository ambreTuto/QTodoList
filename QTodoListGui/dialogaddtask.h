#ifndef DIALOGADDTASK_H
#define DIALOGADDTASK_H

#include <QtGlobal>

#if QT_VERSION >= 0x050000
#include <QtWidgets/QDialog>
#else
#include <QtGui/QDialog>
#endif

namespace Ui {
class DialogAddTask;
}

class DialogAddTask : public QDialog
{
    Q_OBJECT

public:
    explicit DialogAddTask(QWidget *parent = 0);
    ~DialogAddTask();
    QString getTaskName() const;

protected:
    void changeEvent(QEvent *e);

private:
    Ui::DialogAddTask *ui;
};

#endif // DIALOGADDTASK_H
