#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "dialogaddtask.h"
#include "formtask.h"
#include "guiexception.h"
#include "cstoragetype.h"
#include "tasklistmodel.h"

#include <ctodolistmanager.h>
#include <ctaskbo.h>
#include <QMessageBox>
#include <QListWidgetItem>
#include <QStringListModel>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    m_pTodoListManager(new CTodoListManager(CTodoListManager::useTaskDAO(CStorageType::SQL)))
{
    ui->setupUi(this);
    connect(m_pTodoListManager, &CTodoListManager::taskAdded, this, &MainWindow::displayTask);
    connect(m_pTodoListManager, &CTodoListManager::taskRemoved, this, &MainWindow::removeTaskFromBoard);
    connect(m_pTodoListManager, &CTodoListManager::taskUpdated, this, &MainWindow::updateTaskInBoard);
    connect(m_pTodoListManager, &CTodoListManager::failed, this, &MainWindow::displayError);



    modelNewTasks = std::make_shared<TaskListModel>();
    modelOnGoingTasks = std::make_shared<TaskListModel>();
    modelDoneTasks = std::make_shared<TaskListModel>();

    connect(modelOnGoingTasks.get(), &TaskListModel::taskStateChanged, this,
            [&](const qint64 taskId){ m_pTodoListManager->updateTaskState(taskId, CTaskState::ONGOING); });
    connect(modelDoneTasks.get(), &TaskListModel::taskStateChanged, this,
            [&](const qint64 taskId){ m_pTodoListManager->updateTaskState(taskId, CTaskState::DONE); });


    ui->listViewNewTasks->setModel(modelNewTasks.get());
    ui->listViewOnGoingTasks->setModel(modelOnGoingTasks.get());
    ui->listViewDoneTasks->setModel(modelDoneTasks.get());

    QList<CTaskBo> allTask = m_pTodoListManager->getAllTasks();

    for(auto task : allTask)
    {
        displayTask(task);
    }
}

MainWindow::~MainWindow()
{
//    for (int iLoop = 0; iLoop < modelNewTasks->rowCount(); ++iLoop)
//    {
//        const QModelIndex index = modelNewTasks->index(iLoop);
//        const int iNumber = modelNewTasks->data(index, Qt::UserRole + 1).toInt();
//        m_pTodoListManager->updateTaskState(iNumber, CTaskState::NEW);
//    }
//    for (int iLoop = 0; iLoop < modelOnGoingTasks->rowCount(); ++iLoop)
//    {
//        const QModelIndex index = modelOnGoingTasks->index(iLoop);
//        const int iNumber = modelOnGoingTasks->data(index, Qt::UserRole + 1).toInt();
//        m_pTodoListManager->updateTaskState(iNumber, CTaskState::ONGOING);
//    }
//    for (int iLoop = 0; iLoop < modelDoneTasks->rowCount(); ++iLoop)
//    {
//        const QModelIndex index = modelDoneTasks->index(iLoop);
//        const int iNumber = modelDoneTasks->data(index, Qt::UserRole + 1).toInt();
//        m_pTodoListManager->updateTaskState(iNumber, CTaskState::DONE);
//    }
    delete ui;
    delete m_pTodoListManager;
}

void MainWindow::addTask()
{
    DialogAddTask dialogAddTask(this);

    if(QDialog::Accepted == dialogAddTask.exec())
    {
        const QString taskName = dialogAddTask.getTaskName();
        m_pTodoListManager->add(taskName);
    }
}

void MainWindow::displayTask(const CTaskBo &task)
{
    if(task.getState() == CTaskState::NEW)
    {
        const qint64 nbRows = modelNewTasks->rowCount(QModelIndex());
        modelNewTasks->insertRow(static_cast<int>(nbRows));
        const QModelIndex index = modelNewTasks->index(static_cast<int>(nbRows));
        modelNewTasks->setData(index, task.getName(), Qt::DisplayRole);
        modelNewTasks->setData(index, task.getId(), Qt::UserRole + 1);
    }
    else if(task.getState() == CTaskState::ONGOING)
    {
        const qint64 nbRows = modelOnGoingTasks->rowCount(QModelIndex());
        modelOnGoingTasks->insertRow(static_cast<int>(nbRows));
        const QModelIndex index = modelOnGoingTasks->index(static_cast<int>(nbRows));
        modelOnGoingTasks->setData(index, task.getName(), Qt::DisplayRole);
        modelOnGoingTasks->setData(index, task.getId(), Qt::UserRole + 1);
    }
    else if(task.getState() == CTaskState::DONE)
    {
        const qint64 nbRows = modelDoneTasks->rowCount(QModelIndex());
        modelDoneTasks->insertRow(static_cast<int>(nbRows));
        const QModelIndex index = modelDoneTasks->index(static_cast<int>(nbRows));
        modelDoneTasks->setData(index, task.getName(), Qt::DisplayRole);
        modelDoneTasks->setData(index, task.getId(), Qt::UserRole + 1);
    }
}

void MainWindow::displayError(const QString &reason)
{
    QMessageBox::critical(this, QString("error"), reason);
}

void MainWindow::askToRemoveATask(const qint64 taskId)
{
    m_pTodoListManager->removeTaskNumber(taskId);
}
void MainWindow::removeTaskFromBoard(const qint64 taskId)
{
    try
    {
        auto formTask = getFormTaskFromId(taskId);
        disconnect(formTask, &FormTask::removeTaskAsked, this, &MainWindow::askToRemoveATask);
        ui->verticalLayout->removeWidget(formTask);
        formTask->setParent(nullptr);
        formTask->deleteLater();
    }
    catch(QException& e)
    {
        displayError(e.what());
    }
}

void MainWindow::askToEditTask(const CTaskBo &task)
{
    m_pTodoListManager->updateTaskNumber(task);
}

void MainWindow::updateTaskInBoard(const CTaskBo &task)
{
    try
    {
        auto formTask = getFormTaskFromId(task.getId());
        formTask->updateTask(task);
    }
    catch(QException& e)
    {
        displayError(e.what());
    }
}

void MainWindow::changeTaskState(const qint64 taskId)
{
    m_pTodoListManager->updateTaskState(taskId, CTaskState::NEW);

}


FormTask *MainWindow::getFormTaskFromId(const qint64 taskId) const
{
    auto listFormTask = findChildren<FormTask*>();
    auto it = std::find_if(listFormTask.begin(), listFormTask.end(), [&taskId](FormTask* form){ return form->getId() == taskId;} );

    if(it == listFormTask.end())
    {
        GuiException(tr("Task does not exist.")).raise();
    }
    return *it;
}


void MainWindow::changeEvent(QEvent *e)
{
    QMainWindow::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

