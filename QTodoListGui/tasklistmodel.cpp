#include "tasklistmodel.h"
#include "ctaskbo.h"

#include <QMimeData>
#include <QDataStream>

TaskListModel::TaskListModel()
{
}

int TaskListModel::rowCount(const QModelIndex &) const
{
    return m_TaskList.size();
}

QVariant TaskListModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
    {
        return QVariant();
    }

    if (index.row() >= m_TaskList.size())
    {
        return QVariant();
    }

    if (role == Qt::DisplayRole)
    {
        return m_TaskList.at(index.row()).second;
    }
    if (role == Qt::UserRole + 1)
    {
        return m_TaskList[index.row()].first;
    }

    if (role == Qt::TextAlignmentRole)
    {
        return Qt::AlignHCenter;
    }

    return QVariant();
}

bool TaskListModel::insertRows(int position, int rows, const QModelIndex &)
{
    beginInsertRows(QModelIndex(), position, position+rows-1);

    for (int row = 0; row < rows; ++row)
    {
        m_TaskList.insert(position, TaskIdAndTitle(-1, ""));
    }

    endInsertRows();
    return true;
}

bool TaskListModel::setData(const QModelIndex &index,
                              const QVariant &value, int role)
{
    if (index.isValid() == false)
    {
        return false;
    }
    if (role == Qt::DisplayRole)
    {
        m_TaskList[index.row()].second = value.toString();
        emit dataChanged(index, index);
    }
    if (role == Qt::UserRole + 1)
    {
        m_TaskList[index.row()].first = value.toInt();
        emit dataChanged(index, index);
    }
    return true;
}

Qt::DropActions TaskListModel::supportedDropActions() const
{
    return Qt::MoveAction;
}

Qt::ItemFlags TaskListModel::flags(const QModelIndex &index) const
{
    if (!index.isValid())
    {
        return Qt::ItemIsDropEnabled;
    }
    return Qt::ItemIsDragEnabled | Qt::ItemIsDropEnabled | Qt::ItemIsSelectable | Qt::ItemIsEnabled;
}

bool TaskListModel::removeRows(int position, int rows, const QModelIndex &)
{
    beginRemoveRows(QModelIndex(), position, position+rows-1);

    for (int row = 0; row < rows; ++row)
    {
        m_TaskList.removeAt(position);
    }

    endRemoveRows();

    return true;
}

QStringList TaskListModel::mimeTypes() const
{
    QStringList types;
    types << "application/task.data";
    return types;
}

QMimeData *TaskListModel::mimeData(const QModelIndexList &indexes) const
{
    QMimeData *mimeData = new QMimeData();
    QByteArray encodedData;
    QDataStream stream(&encodedData, QIODevice::WriteOnly);

    foreach (const QModelIndex &index, indexes)
    {
        if (index.isValid())
        {
            QString text = data(index, Qt::DisplayRole).toString();
            stream << text;
            stream << data(index, Qt::UserRole + 1).toInt();
        }
    }

    mimeData->setData("application/task.data", encodedData);
    return mimeData;
}

bool TaskListModel::canDropMimeData(const QMimeData *data, Qt::DropAction , int , int column, const QModelIndex &) const
{
    if (!data->hasFormat("application/task.data") || column > 0)
    {
        return false;
    }
    return true;
}

bool TaskListModel::dropMimeData(const QMimeData *data,
    Qt::DropAction action, int row, int column, const QModelIndex &parent)
{
    if (!canDropMimeData(data, action, row, column, parent))
        return false;

    if (action == Qt::IgnoreAction)
        return true;

    int beginRow;

    if (row != -1)
        beginRow = row;
    else if (parent.isValid())
        beginRow = parent.row();
    else
        beginRow = rowCount(QModelIndex());

    QByteArray encodedData = data->data("application/task.data");
    QDataStream stream(&encodedData, QIODevice::ReadOnly);

    while (!stream.atEnd())
    {
        QString text{};
        stream >> text;
        int id{};
        stream >> id;
        insertRow(beginRow, QModelIndex());
        QModelIndex idx = index(beginRow, 0, QModelIndex());
        setData(idx, text, Qt::DisplayRole);
        setData(idx, id, Qt::UserRole + 1);
        beginRow++;
        emit taskStateChanged(id);
    }

    return true;
}

//Qt::ItemFlags TaskListModel::flags(const QModelIndex &index) const
//{
//    if (!index.isValid())
//        return Qt::ItemIsDropEnabled | Qt::ItemIsEnabled;

//    return QAbstractItemModel::flags(index) | Qt::ItemIsDragEnabled | Qt::ItemIsDropEnabled | Qt::ItemIsEditable;
//}

//bool TaskListModel::removeRows(int position, int rows, const QModelIndex &)
//{
//    beginRemoveRows(QModelIndex(), position, position+rows-1);

//    for (int row = 0; row < rows; ++row)
//    {
//        taskList.removeAt(position);
//    }

//    endRemoveRows();
//    return true;
//}


//Qt::DropActions TaskListModel::supportedDropActions() const
//{
//    return Qt::MoveAction;
//}

//QStringList TaskNameListModel::mimeTypes() const
//{
//    QStringList types;
//    types << "application/task.data";
//    return types;
//}

//QMimeData *TaskListModel::mimeData(const QModelIndexList &indexes) const
//{
//    QMimeData *mimeData = new QMimeData();
//    QByteArray encodedData;

//    QDataStream stream(&encodedData, QIODevice::WriteOnly);

//    for (auto &index : indexes)
//    {
//        if (index.isValid() == true)
//        {
//            //QString text = data(index, Qt::DisplayRole).toString();
//            stream <<  taskList.at(index.row());
//        }
//    }

//    mimeData->setData("application/vnd.text.list", encodedData);
//    return mimeData;
//}
