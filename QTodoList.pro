TEMPLATE = subdirs

SUBDIRS += \
    QTodoListBo  \
    QTodoListBLL \
    QtodoListBLL_Test \
    QTodoListDAL \
    QTodoListCLI \
    QTodoListGui

QTodoListDAL.depends = QTodoListBo
QTodoListBLL.depends = QTodoListBo QTodoListDAL
QTodoListCLI.depends = QTodoListBLL
QTodoListGui.depends = QTodoListBLL

QtodoListBLL_Test.depends = QTodoListBLL QTodoListBo
