#include "ctodolistmanager.h"
#include "ctaskbo.h"
#include "cstoragetype.h"

#include <QTextStream>

void writeOn(const QString& sentence,QTextStream& outputStream)
{
    outputStream << sentence << endl;
}

QString ask(const QString& sentence,QTextStream& outputStream,QTextStream& inputStream)
{
    writeOn(sentence, outputStream);
    return inputStream.readLine();
}

void listTaskOn(const CTodoListManager& taskManager,QTextStream& outputStream)
{
    auto index = 0;
    auto strTextToPrint = QString("Tasks created:");

    writeOn(strTextToPrint, outputStream);

    QList<CTaskBo> taskList = taskManager.getAllTasks();

    for (auto& task : taskList)
    {
        strTextToPrint = QString::number(index);
        strTextToPrint += QString(" - ");
        strTextToPrint += task.getName();
        writeOn(strTextToPrint, outputStream);
        index++;
    }
}

int main(int , char *[])
{
    QTextStream out(stdout);
    QTextStream in(stdin);
    QString strTextToPrint("");
    bool leaveApplication = false;
    CTodoListManager taskListManager(CTodoListManager::useTaskDAO(CStorageType::STRINGLIST));

    strTextToPrint = QString("Welcome to QTodo-List application");
    writeOn(strTextToPrint, out);

    do
    {
        strTextToPrint = QString("Please select your action in \"create\", \"list\", \"delete\", \"edit\" or \"quit\"");
        auto action = ask(strTextToPrint, out, in);

        if(action == "create")
        {
            strTextToPrint = QString("Please create your task");
            auto taskName = ask(strTextToPrint, out, in);

            taskListManager.add(taskName);

            strTextToPrint = QString("You created the task ");
            strTextToPrint += taskName;
            writeOn(strTextToPrint, out);
        }
        else if(action == "list")
        {
            listTaskOn(taskListManager, out);
        }
        else if(action == "delete")
        {
            listTaskOn(taskListManager, out);

            strTextToPrint = QString("Which task to delete");
            auto taskToDelete = ask(strTextToPrint, out, in);

            taskListManager.removeTaskNumber(taskToDelete.toInt());
        }
        else if(action == "edit")
        {
            listTaskOn(taskListManager, out);

            strTextToPrint = QString("Which task to rename");
            auto taskToEdit = ask(strTextToPrint, out, in);

            strTextToPrint = QString("Please type the new name");
            auto taskNewName = ask(strTextToPrint, out, in);

            //taskListManager.updateTaskNumber(taskToEdit.toInt(), taskNewName);
        }
        else if(action == "quit")
        {
            leaveApplication = true;
        }

    }while(leaveApplication == false);

    strTextToPrint = QString("Bye bye, see you soon");
    writeOn(strTextToPrint, out);

    return 0;
}





