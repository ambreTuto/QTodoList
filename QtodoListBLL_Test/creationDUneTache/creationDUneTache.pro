QT += testlib
QT -= gui

CONFIG += qt console warn_on depend_includepath testcase
CONFIG -= app_bundle

TEMPLATE = app

SOURCES +=  tst_creationdunetache.cpp

HEADERS += \
    fakeit.hpp

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../../QTodoListBLL/release/ -lQTodoListBLL
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../../QTodoListBLL/debug/ -lQTodoListBLL
else:unix: LIBS += -L$$OUT_PWD/../../QTodoListBLL/ -lQTodoListBLL

INCLUDEPATH += $$PWD/../../QTodoListBLL
DEPENDPATH += $$PWD/../../QTodoListBLL

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../../QTodoListDAL/release/ -lQTodoListDAL
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../../QTodoListDAL/debug/ -lQTodoListDAL
else:unix: LIBS += -L$$OUT_PWD/../../QTodoListDAL/ -lQTodoListDAL

INCLUDEPATH += $$PWD/../../QTodoListDAL
DEPENDPATH += $$PWD/../../QTodoListDAL

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../../QTodoListBo/release/ -lQTodoListBo
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../../QTodoListBo/debug/ -lQTodoListBo
else:unix: LIBS += -L$$OUT_PWD/../../QTodoListBo/ -lQTodoListBo

INCLUDEPATH += $$PWD/../../QTodoListBo
DEPENDPATH += $$PWD/../../QTodoListBo
