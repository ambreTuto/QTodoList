#include <QtTest>

// add necessary includes here
#include "fakeit.hpp"
#include "itaskdao.h"
#include "ctodolistmanager.h"
#include "ctaskbo.h"

class CreationDUneTache : public QObject
{
    Q_OBJECT

public:
    CreationDUneTache();
    ~CreationDUneTache();

private slots:
    void creationDUneTache();
    void creationDUneTacheDejaPresente();
    void creationDUneTacheSansTitre_data();
    void creationDUneTacheSansTitre();

};

CreationDUneTache::CreationDUneTache()
{

}

CreationDUneTache::~CreationDUneTache()
{

}

void CreationDUneTache::creationDUneTache()
{
    // Contexte
    fakeit::Mock<QtodoListDAL::ITaskDAO> mock;
    fakeit::When(Method(mock,getAllTasks)).Return(QList<CTaskBo>());
    fakeit::Fake(Method(mock,add));
    fakeit::Fake(Dtor(mock));

    CTodoListManager todoListManager(std::shared_ptr<QtodoListDAL::ITaskDAO>(&mock.get()));

    // Soit la tache 'Vider la poubelle'
    QString taskName("Vider la poubelle");

    // Quand je crée la tâche "Vider les poubelles"
    todoListManager.add(taskName);
}

void CreationDUneTache::creationDUneTacheDejaPresente()
{
    // Contexte
    fakeit::Mock<QtodoListDAL::ITaskDAO> mock;
    fakeit::Fake(Method(mock,add));
    fakeit::Fake(Dtor(mock));
    CTodoListManager todoListManager(std::shared_ptr<QtodoListDAL::ITaskDAO>(&mock.get()));

    // Contexte
    QString strReason("");
    connect(&todoListManager, &CTodoListManager::failed, [&]( const QString& reason ) { strReason = reason; } );

    // Soit la tache 'Vider la poubelle'
    const QString taskName("Vider la poubelle");
    QList<CTaskBo> taskList;
    taskList.append(CTaskBo(taskName));
    fakeit::When(Method(mock,getAllTasks)).Return(taskList);

    // Quand je crée la tâche "Vider les poubelles"
    todoListManager.add(taskName);

    // Alors une erreur est remontée indiquant que la tâche n'a pas de titre
    QCOMPARE(strReason.isEmpty(), false);
}

void CreationDUneTache::creationDUneTacheSansTitre_data()
{
    QTest::addColumn<QString>("taskName");

    QTest::newRow("vide") << "";
    QTest::newRow("espace")     << "     ";
    QTest::newRow("tabulation") << "        ";
    QTest::newRow("espace et tabulation") << "                ";
}

void CreationDUneTache::creationDUneTacheSansTitre()
{

    fakeit::Mock<QtodoListDAL::ITaskDAO> mock;
    fakeit::Fake(Dtor(mock));
    CTodoListManager todoListManager(std::shared_ptr<QtodoListDAL::ITaskDAO>(&mock.get()));

    // Contexte
    QString strReason("");
    connect(&todoListManager, &CTodoListManager::failed, [&]( const QString& reason ) { strReason = reason; } );

    // une tâche sans titre
    QFETCH(QString, taskName);

    // Quand je la crée
    todoListManager.add(taskName);

    // Alors une erreur est remontée indiquant que la tâche n'a pas de titre
    QCOMPARE(strReason.isEmpty(), false);
}


QTEST_APPLESS_MAIN(CreationDUneTache)

#include "tst_creationdunetache.moc"
