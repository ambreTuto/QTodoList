#include "cdatabaseconnection.h"
#include "ctasksql.h"

#include <ctaskbo.h>
#include <QList>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QVariant>


using namespace QtodoListDAL;

CTaskSql::CTaskSql()
{
    m_pDataBase = new QSqlDatabase(QSqlDatabase::addDatabase("QSQLITE"));
    m_pDataBase->setDatabaseName("QTodoListSqlLite.db");

    CDataBaseConnection dataBaseConnection(*m_pDataBase);
    QSqlQuery query;
    query.exec("CREATE TABLE IF NOT EXISTS task (id INTEGER PRIMARY KEY, description TEXT, state INTEGER)");
}

CTaskSql::~CTaskSql()
{
    delete m_pDataBase;
}

void CTaskSql::add(CTaskBo &task)
{
    CDataBaseConnection dataBaseConnection(*m_pDataBase);
    QSqlQuery query;
    query.prepare("INSERT INTO task (description) VALUES (:description)");
    query.bindValue(":description", task.getName());
    query.exec();
    setTaskId(task, query.lastInsertId().toInt());
}

void CTaskSql::removeTaskNumber(const qint64 taskNumber)
{
    CDataBaseConnection dataBaseConnection(*m_pDataBase);
    QSqlQuery query;
    query.prepare("DELETE FROM task WHERE id = (:id)");
    query.bindValue(":id", taskNumber);
    query.exec();
}

void CTaskSql::updateTaskNumber(const CTaskBo &task)
{
    CDataBaseConnection dataBaseConnection(*m_pDataBase);
    QSqlQuery query;
    query.prepare("UPDATE task SET description = (:description) WHERE id = (:id)");
    query.bindValue(":description", task.getName());
    query.bindValue(":id", task.getId());
    query.exec();
}

QList<CTaskBo> CTaskSql::getAllTasks() const
{
    CDataBaseConnection dataBaseConnection(*m_pDataBase);
    QList<CTaskBo> allTasks;
    QSqlQuery query;
    query.exec("SELECT id, description, state FROM task");

    while (query.next())
    {
        CTaskBo task(query.value(1).toString());
        setTaskId(task, query.value(0).toInt());
        setTaskState(task, static_cast<CTaskState>(query.value(2).toInt()));
        allTasks << task;
    }
    return allTasks;
}

void CTaskSql::updateTaskState(const qint64 taskNumber, const CTaskState state)
{
    CDataBaseConnection dataBaseConnection(*m_pDataBase);
    QSqlQuery query;
    query.prepare("UPDATE task SET state = (:state) WHERE id = (:id)");
    query.bindValue(":state", static_cast<int>(state));
    query.bindValue(":id", taskNumber);
    query.exec();
}

