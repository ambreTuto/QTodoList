#ifndef ITASKDAO_H
#define ITASKDAO_H

#include "qtodolistdal_global.h"

#include <QtCore/qglobal.h>

enum class CTaskState;
class CTaskBo;

namespace QtodoListDAL
{
    class QTODOLISTDALSHARED_EXPORT ITaskDAO
    {
    public:
        ITaskDAO();
        virtual ~ITaskDAO();
        virtual void add(CTaskBo& task) = 0;
        virtual void removeTaskNumber(const qint64 taskNumber) = 0;
        virtual void updateTaskNumber(const CTaskBo& task) = 0;
        virtual void updateTaskState(const qint64 taskNumber, const CTaskState state){};
        virtual QList<CTaskBo> getAllTasks() const = 0;
    protected:
        void setTaskId(CTaskBo& task, const qint64 taskNumber) const;
        void setTaskState(CTaskBo& task, const CTaskState& taskState) const;
    };
}
#endif // ITASKDAO_H
