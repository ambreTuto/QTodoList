#ifndef CTASKDAO_H
#define CTASKDAO_H

#include "itaskdao.h"
#include "qtodolistdal_global.h"
#include <QtCore/qglobal.h>
#include <QList>

class CTaskBo;

namespace QtodoListDAL
{

    class QTODOLISTDALSHARED_EXPORT CTaskDao : public ITaskDAO
    {
    public:
        CTaskDao();

        void add(CTaskBo& task) override;
        void removeTaskNumber(const qint64 taskNumber) override;
        void updateTaskNumber(const CTaskBo& task) override;
        QList<CTaskBo> getAllTasks() const override;
        void updateTaskState(const qint64 taskNumber, const CTaskState state) override;

    protected:

    private:
        QList<CTaskBo> m_TaskList;
        qint64 m_Id;
    };

}
#endif // CTASKSQL_H
