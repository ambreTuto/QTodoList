#ifndef CTASKSQL_H
#define CTASKSQL_H

#include "itaskdao.h"
#include "qtodolistdal_global.h"
#include <QtCore/qglobal.h>

class QSqlDatabase;

namespace QtodoListDAL
{

    class QTODOLISTDALSHARED_EXPORT CTaskSql : public ITaskDAO
    {
    public:
        CTaskSql();
        ~CTaskSql() override;


        // ITaskDAO interface
        void add(CTaskBo& task) override;
        void removeTaskNumber(const qint64 taskNumber) override;
        void updateTaskNumber(const CTaskBo& task) override;
        QList<CTaskBo> getAllTasks() const override;
        void updateTaskState(const qint64 taskNumber, const CTaskState state) override;

    protected:

    private:
        QSqlDatabase* m_pDataBase;

    };

}
#endif // CTASKSQL_H
