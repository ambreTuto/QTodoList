#include "itaskdao.h"

#include <ctaskbo.h>

using namespace QtodoListDAL;

ITaskDAO::ITaskDAO()
{

}

ITaskDAO::~ITaskDAO()
{

}

void ITaskDAO::setTaskId(CTaskBo &task, const qint64 taskNumber) const
{
    task.setId(taskNumber);

}

void ITaskDAO::setTaskState(CTaskBo &task, const CTaskState& taskState) const
{
    task.setState(taskState);

}

