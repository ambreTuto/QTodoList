#ifndef CDATABASECONNECTION_H
#define CDATABASECONNECTION_H

#include <QtCore/qglobal.h>

class QSqlDatabase;

class CDataBaseConnection
{
public:
    CDataBaseConnection(QSqlDatabase& database);
    ~CDataBaseConnection();

private:
    QSqlDatabase& m_DataBase;
};

#endif // CDATABASECONNECTION_H
