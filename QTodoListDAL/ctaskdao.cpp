#include "ctaskdao.h"
#include "ctaskbo.h"

using namespace QtodoListDAL;

CTaskDao::CTaskDao()
    : m_Id(0)
{
}

void CTaskDao::add( CTaskBo& task)
{
    m_TaskList << task;
    setTaskId(task, m_Id);
    m_Id++;
}

QList<CTaskBo> CTaskDao::getAllTasks() const
{
    return m_TaskList;
}

void CTaskDao::updateTaskState(const qint64 taskNumber, const CTaskState state)
{
    for(auto& task : m_TaskList)
    {
        if(task.getId() == taskNumber)
        {
            setTaskState(task, state);
        }
    }
   // /*auto taskFound = */std::find(m_TaskList.begin(), m_TaskList.end(), [](CTaskBo& task){return true/*task.getId() == taskNumber*/;});
//    if(taskFound != m_TaskList.end())
//    {
//        setTaskState(*taskFound, state);
//    }
}

void CTaskDao::removeTaskNumber(const qint64 taskNumber)
{
    m_TaskList.erase(std::remove_if(m_TaskList.begin(),
                                    m_TaskList.end(),
                                    [=](CTaskBo& task){return task.getId() == taskNumber; }),
                        m_TaskList.end());
}

void CTaskDao::updateTaskNumber(const CTaskBo &task)
{
    std::replace_if( m_TaskList.begin(),
                     m_TaskList.end(),
                     [&](CTaskBo& currentTask) {return task.getId() == currentTask.getId();},
                     task);
}
