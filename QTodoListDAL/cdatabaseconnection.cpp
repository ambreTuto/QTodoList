#include "cdatabaseconnection.h"

#include <QSqlDatabase>

CDataBaseConnection::CDataBaseConnection(QSqlDatabase &database)
    : m_DataBase(database)
{
    if(m_DataBase.isOpen() == false)
    {
        m_DataBase.open();
    }
}

CDataBaseConnection::~CDataBaseConnection()
{
    if(m_DataBase.isOpen() == true)
    {
        m_DataBase.close();
    }
}
