#include "ctaskbo.h"

#include <QRegularExpression>
#include <QDataStream>

CTaskBo::CTaskBo(const QString& strName)
    : m_TaskName(strName),
      m_Id(-1),
      m_State(CTaskState::NEW)
{

}

void CTaskBo::setName(const QString &name)
{
    m_TaskName = name;
}

QString CTaskBo::getName() const
{
    return m_TaskName;
}

qint64 CTaskBo::getId() const
{
    return m_Id;
}

qint64 CTaskBo::getOnGoing() const
{
    return 0;
}

QString CTaskBo::getDescription() const
{
    return "";
}

QStringList CTaskBo::getCommentsList() const
{
    return m_CommentsList;
}

QDate CTaskBo::getCreationDate() const
{
    return creationDate;
}

QDate CTaskBo::getLastModificationDate() const
{
    return lastModificationDate;
}

CTaskState CTaskBo::getState() const
{
    return m_State;
}

QDataStream &operator<<(QDataStream &stream, const CTaskBo &task)
{
    stream << task.getId();
    return stream;
}

QDataStream &operator>>(QDataStream &stream, CTaskBo &task)
{
    return stream;
}

void CTaskBo::setId(qint64 id)
{
    m_Id = id;
}

void CTaskBo::setState(const CTaskState &state)
{
    m_State = state;
}
