#ifndef CTASKBO_H
#define CTASKBO_H

#include "qtodolistbo_global.h"
#include "CTaskState.h"

#include <QString>
#include <QStringList>
#include <QDate>


namespace QtodoListDAL
{
    class ITaskDAO;
}


class QTODOLISTBOSHARED_EXPORT CTaskBo
{
    friend class QtodoListDAL::ITaskDAO;

public:
    /// Methods
    CTaskBo(const QString& strName);
    void setName(const QString& name);
    QString getName() const;
    //void setName(const QString name):
    qint64 getId() const;
    qint64 getOnGoing() const;
    QString getDescription() const;
    QStringList getCommentsList() const;
    QDate getCreationDate() const;
    QDate getLastModificationDate() const;
    CTaskState getState() const;


private:

    /// Methods
    void setId(qint64 id);
    void setState(const CTaskState& state);

    /// Attributs
    QString m_TaskName;
    qint64 m_Id;
    QString m_Description;
    QStringList m_CommentsList;
    qint64 m_Percent;
    QDate creationDate;
    QDate lastModificationDate;
    CTaskState m_State;


};

QTODOLISTBOSHARED_EXPORT QDataStream &operator<<(QDataStream &stream, const CTaskBo &task);
QTODOLISTBOSHARED_EXPORT QDataStream &operator>>(QDataStream &stream, CTaskBo &task);

#endif // CTASKBO_H
