#ifndef CTASKSTATE_H
#define CTASKSTATE_H

#include "qtodolistbo_global.h"

enum class CTaskState
{
    NEW,
    ONGOING,
    DONE
};


#endif // CTASKSTATE_H
