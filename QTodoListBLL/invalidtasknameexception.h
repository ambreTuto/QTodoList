#ifndef INVALIDTASKNAMEEXCEPTION_H
#define INVALIDTASKNAMEEXCEPTION_H

#include <QtCore/qglobal.h>
#include <QException>

class InvalidTaskNameException : public QException
{
public:
    InvalidTaskNameException(const QString& description);
    const char *what() const _GLIBCXX_USE_NOEXCEPT override;

private:
    const QString m_description;
};

#endif // INVALIDTASKNAMEEXCEPTION_H
