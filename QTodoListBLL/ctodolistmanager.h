#ifndef CTODOLISTMANAGER_H
#define CTODOLISTMANAGER_H

#include "qtodolistbll_global.h"

#include <QObject>
#include <memory>

namespace QtodoListDAL
{
    class ITaskDAO;
}

class CTaskBo;
enum class CStorageType;
enum class CTaskState;

class QTODOLISTBLLSHARED_EXPORT CTodoListManager : public QObject
{
    Q_OBJECT

public:
    CTodoListManager(std::shared_ptr<QtodoListDAL::ITaskDAO> taskDAO);
    ~CTodoListManager();
    void add(const QString& taskName);
    QList<CTaskBo> getAllTasks() const;
    void removeTaskNumber(const qint64 taskId);
    void updateTaskNumber(const CTaskBo& task);
    void updateTaskState(const qint64 taskId, const CTaskState state);
    static std::shared_ptr<QtodoListDAL::ITaskDAO> useTaskDAO(const CStorageType& storageType);

Q_SIGNALS:
    void taskAdded(const CTaskBo& task);
    void taskRemoved(const qint64 id);
    void taskUpdated(const CTaskBo& task);
    void failed(const QString& reason);

private:
    std::shared_ptr<QtodoListDAL::ITaskDAO> m_TaskDao;
    void checkIfNameValid(const QString& taskName) const;
    void checkIfNameNotPresent(const QString& taskName) const;

};

#endif // CTODOLISTMANAGER_H

