#include "ctodolistmanager.h"
#include "ctaskbo.h"
#include "ctaskdao.h"
#include "ctasksql.h"
#include "cstoragetype.h"
#include "invalidtasknameexception.h"
#include "taskalreadypresentexception.h"

#include <QException>
#include <QRegularExpression>

CTodoListManager::CTodoListManager(std::shared_ptr<QtodoListDAL::ITaskDAO> taskDAO)
    : m_TaskDao(taskDAO)
{

}


CTodoListManager::~CTodoListManager()
{
}

void CTodoListManager::add(const QString &taskName)
{
    try
    {
        CTaskBo task(taskName);
        if(m_TaskDao)
        {
            checkIfNameValid(taskName);
            checkIfNameNotPresent(taskName);
            m_TaskDao->add(task);
        }
        emit taskAdded(task);
    }
    catch (QException &e)
    {
        emit failed(e.what());
    }
}

void CTodoListManager::checkIfNameValid(const QString &taskName) const
{
    QRegularExpression re("^[\\s|\\t]*$");
    QRegularExpressionMatch match = re.match(taskName);
    if(match.hasMatch() == true)
    {
        throw InvalidTaskNameException("Le nom de la tâche ne doit pas être vide");
    }
}

void CTodoListManager::checkIfNameNotPresent(const QString &taskName) const
{
    QList<CTaskBo> allTask = getAllTasks();
    auto it = std::find_if(allTask.begin(), allTask.end(), [&taskName](const CTaskBo& task)
    {
        return task.getName() == taskName;
    });

    if(it != allTask.end())
    {
        throw TaskAlreadyPresentException(taskName);
    }
}


QList<CTaskBo> CTodoListManager::getAllTasks() const
{
    if(m_TaskDao)
    {
        return m_TaskDao->getAllTasks();
    }
    return QList<CTaskBo>();
}

void CTodoListManager::removeTaskNumber(const qint64 taskId)
{
    try
    {
        m_TaskDao->removeTaskNumber(taskId);
        emit taskRemoved(taskId);
    }
    catch (QException &e)
    {
        emit failed(e.what());
    }
}

void CTodoListManager::updateTaskNumber(const CTaskBo& task)
{
    try
    {
        m_TaskDao->updateTaskNumber(task);
        emit taskUpdated(task);
    }
    catch (QException &e)
    {
        emit failed(e.what());
    }
}

void CTodoListManager::updateTaskState(const qint64 taskId, const CTaskState state)
{
    m_TaskDao->updateTaskState(taskId, state);
}

std::shared_ptr<QtodoListDAL::ITaskDAO> CTodoListManager::useTaskDAO(const CStorageType &storageType)
{
    if(storageType == CStorageType::STRINGLIST)
    {
        return std::make_shared<QtodoListDAL::CTaskDao>();
    }
    else if(storageType == CStorageType::SQL)
    {
        return std::make_shared<QtodoListDAL::CTaskSql>();
    }
    return nullptr;
}
