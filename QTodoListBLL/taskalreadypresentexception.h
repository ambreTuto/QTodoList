#ifndef TASKALREADYPRESENTEXCEPTION_H
#define TASKALREADYPRESENTEXCEPTION_H

#include <QException>

class TaskAlreadyPresentException : public QException
{
public:
    TaskAlreadyPresentException(const QString& taskName);
    const char* what() const _GLIBCXX_USE_NOEXCEPT override;

private:
    const QString& m_TaskName;
};

#endif // TASKALREADYPRESENTEXCEPTION_H
