#include "taskalreadypresentexception.h"

TaskAlreadyPresentException::TaskAlreadyPresentException(const QString& taskName)
    : m_TaskName(taskName)
{

}

const char* TaskAlreadyPresentException::what() const noexcept
{
    return QString("La tâche " + m_TaskName + " est déjà présente").toLatin1().data();
}

