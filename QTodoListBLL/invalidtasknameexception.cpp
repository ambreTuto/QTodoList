#include "invalidtasknameexception.h"

InvalidTaskNameException::InvalidTaskNameException(const QString& description)
    : m_description(description)
{

}

const char* InvalidTaskNameException::what() const noexcept
{
    return m_description.toLatin1().data();
}


