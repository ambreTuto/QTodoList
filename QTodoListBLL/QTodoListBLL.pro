#-------------------------------------------------
#
# Project created by QtCreator 2017-03-20T11:02:11
#
#-------------------------------------------------

include ( ../QTodoListDll.pri )

QT       -= gui

TARGET = QTodoListBLL
TEMPLATE = lib

CONFIG += c++14

DEFINES += QTODOLISTBLL_LIBRARY

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    ctodolistmanager.cpp \
    taskalreadypresentexception.cpp \
    invalidtasknameexception.cpp

HEADERS +=\
        qtodolistbll_global.h \
    ctodolistmanager.h \
    taskalreadypresentexception.h \
    cstoragetype.h \
    invalidtasknameexception.h


win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../QTodoListDAL/release/ -lQTodoListDAL
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../QTodoListDAL/debug/ -lQTodoListDAL
else:unix: LIBS += -L$$OUT_PWD/../QTodoListDAL/ -lQTodoListDAL

INCLUDEPATH += $$PWD/../QTodoListDAL
DEPENDPATH += $$PWD/../QTodoListDAL

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../QTodoListBo/release/ -lQTodoListBo
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../QTodoListBo/debug/ -lQTodoListBo
else:unix: LIBS += -L$$OUT_PWD/../QTodoListBo/ -lQTodoListBo

INCLUDEPATH += $$PWD/../QTodoListBo
DEPENDPATH += $$PWD/../QTodoListBo

