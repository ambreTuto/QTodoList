#ifndef CSTORAGETYPE_H
#define CSTORAGETYPE_H

#include "qtodolistbll_global.h"

enum class CStorageType
{
    STRINGLIST,
    SQL,
    XML
};

#endif // CSTORAGETYPE_H
