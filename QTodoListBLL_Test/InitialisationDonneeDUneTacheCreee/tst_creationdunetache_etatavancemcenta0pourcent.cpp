//#include "CTodoListManager.h"

#include <QtTest>

// add necessary includes here
#include <memory>
#include "itaskdao.h"
#include "ctodolistmanager.h"
#include "ctaskbo.h"

class MyStorage : public QtodoListDAL::ITaskDAO
{
public:
    virtual void add(CTaskBo& )
    {

    }
    virtual void removeTaskNumber(const qint64 )
    {

    }
    virtual void updateTaskNumber(const CTaskBo& )
    {

    }
    virtual QList<CTaskBo> getAllTasks() const
    {
        return QList<CTaskBo>();
    }
};

class InitialisationDesDonneesDUneTacheCreee : public QObject
{
    Q_OBJECT

public:
    InitialisationDesDonneesDUneTacheCreee();
    ~InitialisationDesDonneesDUneTacheCreee();

private slots:
    void etatAvancement0Pourcent();
    void descriptionEstVide();
    void listeDeCommentaireEstVide();
    void dateDeCreationEgalDateModification();
    void etatDeLaTacheEstNouveau();


};

InitialisationDesDonneesDUneTacheCreee::InitialisationDesDonneesDUneTacheCreee()
{

}

InitialisationDesDonneesDUneTacheCreee::~InitialisationDesDonneesDUneTacheCreee()
{

}

void InitialisationDesDonneesDUneTacheCreee::etatAvancement0Pourcent()
{
    std::shared_ptr<MyStorage> myStorage = std::make_shared<MyStorage>();
    CTodoListManager todoListManager(myStorage);

    // Contexte
    qint64 onGoingValue;
    connect(&todoListManager, &CTodoListManager::taskAdded, [&]( const CTaskBo &task ) { onGoingValue = task.getOnGoing(); } );

    // Soit la tache 'Vider la poubelle'
    QString taskName("Vider la poubelle");

    // Quand je la crée
    todoListManager.add(taskName);

    // Alors son état d'avancement est de '0'
    QCOMPARE(onGoingValue, 0);
}

void InitialisationDesDonneesDUneTacheCreee::descriptionEstVide()
{
    std::shared_ptr<MyStorage> myStorage = std::make_shared<MyStorage>();
    CTodoListManager todoListManager(myStorage);

    // Contexte
    QString description;
    connect(&todoListManager, &CTodoListManager::taskAdded, [&]( const CTaskBo &task ) { description = task.getDescription(); } );

    // Soit la tache 'Vider la poubelle'
    QString taskName("Vider la poubelle");

    // Quand je la crée
    todoListManager.add(taskName);

    // Alors sa description est vide
    QCOMPARE(description.isEmpty(), true);
}

void InitialisationDesDonneesDUneTacheCreee::listeDeCommentaireEstVide()
{
    std::shared_ptr<MyStorage> myStorage = std::make_shared<MyStorage>();
    CTodoListManager todoListManager(myStorage);

    // Contexte
    QStringList listeDeCommentaires;
    connect(&todoListManager, &CTodoListManager::taskAdded, [&]( const CTaskBo &task ) { listeDeCommentaires = task.getCommentsList(); } );

    // Soit la tache 'Vider la poubelle'
    QString taskName("Vider la poubelle");

    // Quand je la crée
    todoListManager.add(taskName);

    // Alors sa description est vide
    QCOMPARE(listeDeCommentaires.isEmpty(), true);
}

void InitialisationDesDonneesDUneTacheCreee::dateDeCreationEgalDateModification()
{
    std::shared_ptr<MyStorage> myStorage = std::make_shared<MyStorage>();
    CTodoListManager todoListManager(myStorage);

    // Contexte
    QDate dateDeCreation;
    QDate dateDerniereModification;
    connect(&todoListManager, &CTodoListManager::taskAdded, [&]( const CTaskBo &task )
    {
        dateDeCreation = task.getCreationDate();
        dateDerniereModification = task.getLastModificationDate();
    } );

    // Soit la tache 'Vider la poubelle'
    QString taskName("Vider la poubelle");

    // Quand je la crée
    todoListManager.add(taskName);

    // Alors sa description est vide
    QCOMPARE(dateDeCreation, dateDerniereModification);
}

void InitialisationDesDonneesDUneTacheCreee::etatDeLaTacheEstNouveau()
{
    std::shared_ptr<MyStorage> myStorage = std::make_shared<MyStorage>();
    CTodoListManager todoListManager(myStorage);

    // Contexte
    CTaskState taskState;
    connect(&todoListManager, &CTodoListManager::taskAdded, [&]( const CTaskBo &task )
    {
        taskState = task.getState();
    } );

    // Soit la tache 'Vider la poubelle'
    QString taskName("Vider la poubelle");

    // Quand je la crée
    todoListManager.add(taskName);

    // Alors sa description est vide
    QCOMPARE(taskState, CTaskState::NEW);
}

QTEST_APPLESS_MAIN(InitialisationDesDonneesDUneTacheCreee)

#include "tst_creationdunetache_etatavancemcenta0pourcent.moc"
