#-------------------------------------------------
#
# Project created by QtCreator 2017-10-16T17:53:32
#
#-------------------------------------------------


include ( ../QTodoListBin.pri )

QT       += testlib

QT       -= gui

TARGET = tst_initialiserlesdonneesdunetachecreeetest
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
        tst_initialiserlesdonneesdunetachecreeetest.cpp 

DEFINES += SRCDIR=\\\"$$PWD/\\\"

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../QTodoListBLL/release/ -lQTodoListBLL
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../QTodoListBLL/debug/ -lQTodoListBLL
else:unix: LIBS += -L$$OUT_PWD/../QTodoListBLL/ -lQTodoListBLL

INCLUDEPATH += $$PWD/../QTodoListBLL
DEPENDPATH += $$PWD/../QTodoListBLL

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../QTodoListBo/release/ -lQTodoListBo
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../QTodoListBo/debug/ -lQTodoListBo
else:unix: LIBS += -L$$OUT_PWD/../QTodoListBo/ -lQTodoListBo

INCLUDEPATH += $$PWD/../QTodoListBo
DEPENDPATH += $$PWD/../QTodoListBo

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../QTodoListDAL/release/ -lQTodoListDAL
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../QTodoListDAL/debug/ -lQTodoListDAL
else:unix: LIBS += -L$$OUT_PWD/../QTodoListDAL/ -lQTodoListDAL

INCLUDEPATH += $$PWD/../QTodoListDAL
DEPENDPATH += $$PWD/../QTodoListDAL
