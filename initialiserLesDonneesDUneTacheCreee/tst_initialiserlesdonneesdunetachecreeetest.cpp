#include "ctodolistmanager.h"
#include "ctaskbo.h"
#include "ctaskdao.h"

#include <QString>
#include <QtTest>

class MyStorage : public QtodoListDAL::ITaskDAO
{
public:
    virtual void add(CTaskBo& )
    {

    }
    virtual void removeTaskNumber(const qint64 )
    {

    }
    virtual void updateTaskNumber(const CTaskBo& )
    {

    }
    virtual QList<CTaskBo> getAllTasks() const
    {
        return QList<CTaskBo>();
    }
};

class InitialiserLesDonneesDUneTacheCreeeTest : public QObject
{
    Q_OBJECT

public:
    InitialiserLesDonneesDUneTacheCreeeTest();
    virtual ~InitialiserLesDonneesDUneTacheCreeeTest();

private:

private Q_SLOTS:
    void etatAvancement0Pourcent();
    void descriptionEstVide();
    void listeDeCommentaireEstVide();
    void dateDeCreationEgalDateModification();

};

InitialiserLesDonneesDUneTacheCreeeTest::InitialiserLesDonneesDUneTacheCreeeTest()
{
}

InitialiserLesDonneesDUneTacheCreeeTest::~InitialiserLesDonneesDUneTacheCreeeTest()
{
}

void InitialiserLesDonneesDUneTacheCreeeTest::etatAvancement0Pourcent()
{

    std::shared_ptr<MyStorage> myStorage = std::make_shared<MyStorage>();
    CTodoListManager todoListManager(myStorage);

    // Contexte
    qint64 onGoingValue;
    connect(&todoListManager, &CTodoListManager::taskAdded, [&]( const CTaskBo &task ) { onGoingValue = task.getOnGoing(); } );

    // Soit la tache 'Vider la poubelle'
    QString taskName("Vider la poubelle");

    // Quand je la crée
    todoListManager.add(taskName);

    // Alors son état d'avancement est de '0'
    QCOMPARE(onGoingValue, 0);
}

void InitialiserLesDonneesDUneTacheCreeeTest::descriptionEstVide()
{
    std::shared_ptr<MyStorage> myStorage = std::make_shared<MyStorage>();
    CTodoListManager todoListManager(myStorage);

    // Contexte
    QString description;
    connect(&todoListManager, &CTodoListManager::taskAdded, [&]( const CTaskBo &task ) { description = task.getDescription(); } );

    // Soit la tache 'Vider la poubelle'
    QString taskName("Vider la poubelle");

    // Quand je la crée
    todoListManager.add(taskName);

    // Alors sa description est vide
    QCOMPARE(description.isEmpty(), true);
}

void InitialiserLesDonneesDUneTacheCreeeTest::listeDeCommentaireEstVide()
{
    std::shared_ptr<MyStorage> myStorage = std::make_shared<MyStorage>();
    CTodoListManager todoListManager(myStorage);

    // Contexte
    QStringList listeDeCommentaires;
    connect(&todoListManager, &CTodoListManager::taskAdded, [&]( const CTaskBo &task ) { listeDeCommentaires = task.getCommentsList(); } );

    // Soit la tache 'Vider la poubelle'
    QString taskName("Vider la poubelle");

    // Quand je la crée
    todoListManager.add(taskName);

    // Alors sa description est vide
    QCOMPARE(listeDeCommentaires.isEmpty(), true);
}

void InitialiserLesDonneesDUneTacheCreeeTest::dateDeCreationEgalDateModification()
{
    std::shared_ptr<MyStorage> myStorage = std::make_shared<MyStorage>();
    CTodoListManager todoListManager(myStorage);

    // Contexte
    QDate dateDeCreation;
    QDate dateDerniereModification;
    connect(&todoListManager, &CTodoListManager::taskAdded, [&]( const CTaskBo &task )
    {
        dateDeCreation = task.getCreationDate();
        dateDerniereModification = task.getLastModificationDate();
    } );

    // Soit la tache 'Vider la poubelle'
    QString taskName("Vider la poubelle");

    // Quand je la crée
    todoListManager.add(taskName);

    // Alors sa description est vide
    QCOMPARE(dateDeCreation, dateDerniereModification);
}
QTEST_APPLESS_MAIN(InitialiserLesDonneesDUneTacheCreeeTest)

#include "tst_initialiserlesdonneesdunetachecreeetest.moc"
